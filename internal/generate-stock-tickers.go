package internal

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/andiemen/generate-stock-ticker-and-price/initializers"
	"gitlab.com/andiemen/generate-stock-ticker-and-price/pkg/models"
)

// Generate a ticker name
func generateTickerName() string {
	sourceCharacters := "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	src := rand.NewSource(time.Now().UnixNano())
	r := rand.New(src)
	limit := r.Intn(4-(1+1)) + 1

	resultTicker := ""
	for i := 0; i < limit; i++ {
		randomInt := r.Intn(len(sourceCharacters))
		resultTicker += string(sourceCharacters[randomInt])
	}
	return resultTicker
}

// Generates n random ticker names and stores them in DB
// if there are no stock tickers
// generate and insert n of them
func GenerateStockTickers(n int) {
	var stocks []models.Stock
	//get all stocks
	result := initializers.DB.Find(&stocks)
	if int(result.RowsAffected) > n {
		return
	}
	limit := n - int(result.RowsAffected)
	for i := 0; i < limit; i++ {
		tickerName := generateTickerName()
		fmt.Println(tickerName)
		stock := models.Stock{Ticker: tickerName}
		fmt.Println(stock)
		insertResult := initializers.DB.Create(&stock)
		if insertResult.Error != nil {
			i--
		}
	}
}
