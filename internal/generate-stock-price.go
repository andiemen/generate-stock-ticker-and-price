package internal

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/andiemen/generate-stock-ticker-and-price/initializers"
	"gitlab.com/andiemen/generate-stock-ticker-and-price/pkg/models"
)

// Store Prices
func storePrices(prices []models.Price) {
	initializers.DB.CreateInBatches(&prices, 10)
}

// Generate and store stock prices in DB
func generateStockPrice()  {
	// Get distinct on stockID from prices
	// insert new rows with new prices

	src := rand.NewSource(time.Now().UnixNano())
	r := rand.New(src)

	var stockPrices []models.StockPrice
	priceSubQuery := initializers.DB.Model(
		&models.Price{},
		).Select(
			"created_at, current",
		).Where(
			"stock_id = stocks.id",
		).Order(
			"created_at DESC",
		).Limit(1)
	result := initializers.DB.Model(
		&models.Stock{},
		).Select(
			"stocks.id, stocks.ticker, COALESCE(prices.current, 0) as price, prices.created_at as last_update",
			).Joins(
				"LEFT JOIN LATERAL (?) prices on TRUE",
				priceSubQuery,
				).Scan(&stockPrices)

	pricesToInsert := make([]models.Price, result.RowsAffected)

	var current float32
	for i, stockPrice := range stockPrices {
		// Random 0 or 1
		randomInt := r.Intn(2)
		switch randomInt {
			case 0:
				current = 0
				if stockPrice.Price > 0 {
					current = stockPrice.Price - 1
				}
			case 1:
				current = stockPrice.Price + 1
		}
		pricesToInsert[i] = models.Price{Current: current, StockID: stockPrice.ID}
	}
	storePrices(pricesToInsert)
}

// Generate and store stock prices in DB every second
func GenerateStockPrice() {
	fmt.Println("Generating StockPrices every 1 * time.Second")
	ticker := time.NewTicker(1 * time.Second)
	quit := make(chan struct{})
	go func() {
		for {
			select {
				case <-ticker.C:
					generateStockPrice()
				case <-quit:
					ticker.Stop()
					return
			}
		}
	}()
}