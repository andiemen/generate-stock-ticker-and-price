package main

import (
	"fmt"
	"os"

	"gitlab.com/andiemen/generate-stock-ticker-and-price/pkg/cmd"
	"gitlab.com/andiemen/generate-stock-ticker-and-price/initializers"
	"gitlab.com/andiemen/generate-stock-ticker-and-price/internal"
)

func init() {
	initializers.LoadEnvVariables()
	initializers.ConnectToDB()
}

func main() {
	internal.GenerateStockTickers(100)
	internal.GenerateStockPrice()
	if err := cmd.RunServer(); err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}
}