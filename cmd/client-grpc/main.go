package main

import (
	"context"
	"flag"
	"log"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"gitlab.com/andiemen/generate-stock-ticker-and-price/pkg/api/v1"
)

const (
	// apiVersion is version of API is provided by server
	apiVersion = "v1"
)

func main() {
	// get configuration
	address := flag.String("server", "", "gRPC server in format host:port")
	flag.Parse()

	// Set up a connection to the server.
	conn, err := grpc.Dial(*address, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	c := v1.NewStocksServiceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// Call GetStocks
	req1 := v1.GetStocksRequest{
		Api: apiVersion,
	}
	res4, err := c.GetStocks(ctx, &req1)
	if err != nil {
		log.Fatalf("GetStocks failed: %v", err)
	}
	log.Printf("GetStocks result: <%+v>\n\n", res4)

}