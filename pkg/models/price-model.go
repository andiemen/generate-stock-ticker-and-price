package models

import (
	"time"
)

type Price struct {
	ID					int
  Current			float32
  StockID			int 			`gorm:"index:idx_stock_id_created_at"`
  CreatedAt		time.Time	`gorm:"index:idx_stock_id_created_at"`
	Stock 			Stock 		`gorm:"constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
}