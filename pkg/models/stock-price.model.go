package models

import "time"

type StockPrice struct {
	ID          int
	Ticker      string
	Price       float32
	Description string
	LastUpdate  time.Time
}
