package models

import (
	"gorm.io/gorm"
)

type Stock struct {
	gorm.Model
	Ticker 			string `gorm:"unique"`
	Description string
}
