// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.21.11
// source: stock-generator-service.proto

package v1

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	timestamppb "google.golang.org/protobuf/types/known/timestamppb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// Stock we return
type StockPrice struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Unique integer identifier of a stock
	Id int64 `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	// Ticker of a the stock
	Ticker string `protobuf:"bytes,2,opt,name=ticker,proto3" json:"ticker,omitempty"`
	// Price of the stock
	Price float32 `protobuf:"fixed32,3,opt,name=price,proto3" json:"price,omitempty"`
	// Detail description of the stock
	Description string `protobuf:"bytes,4,opt,name=description,proto3" json:"description,omitempty"`
	// Date and time of last stock price
	LastUpdate *timestamppb.Timestamp `protobuf:"bytes,5,opt,name=lastUpdate,proto3" json:"lastUpdate,omitempty"`
}

func (x *StockPrice) Reset() {
	*x = StockPrice{}
	if protoimpl.UnsafeEnabled {
		mi := &file_stock_generator_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StockPrice) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StockPrice) ProtoMessage() {}

func (x *StockPrice) ProtoReflect() protoreflect.Message {
	mi := &file_stock_generator_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StockPrice.ProtoReflect.Descriptor instead.
func (*StockPrice) Descriptor() ([]byte, []int) {
	return file_stock_generator_service_proto_rawDescGZIP(), []int{0}
}

func (x *StockPrice) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *StockPrice) GetTicker() string {
	if x != nil {
		return x.Ticker
	}
	return ""
}

func (x *StockPrice) GetPrice() float32 {
	if x != nil {
		return x.Price
	}
	return 0
}

func (x *StockPrice) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

func (x *StockPrice) GetLastUpdate() *timestamppb.Timestamp {
	if x != nil {
		return x.LastUpdate
	}
	return nil
}

// Request data to get stocks
type GetStocksRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// API versioning: I've seen best practice
	// where version is explicitly specified
	Api string `protobuf:"bytes,1,opt,name=api,proto3" json:"api,omitempty"`
}

func (x *GetStocksRequest) Reset() {
	*x = GetStocksRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_stock_generator_service_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetStocksRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetStocksRequest) ProtoMessage() {}

func (x *GetStocksRequest) ProtoReflect() protoreflect.Message {
	mi := &file_stock_generator_service_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetStocksRequest.ProtoReflect.Descriptor instead.
func (*GetStocksRequest) Descriptor() ([]byte, []int) {
	return file_stock_generator_service_proto_rawDescGZIP(), []int{1}
}

func (x *GetStocksRequest) GetApi() string {
	if x != nil {
		return x.Api
	}
	return ""
}

// Response with a list of stocks
type GetStocksResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// API versioning: I've seen best practice
	// where version is explicitly specified
	Api string `protobuf:"bytes,1,opt,name=api,proto3" json:"api,omitempty"`
	// Array of Stock entities
	Stocks []*StockPrice `protobuf:"bytes,2,rep,name=stocks,proto3" json:"stocks,omitempty"`
}

func (x *GetStocksResponse) Reset() {
	*x = GetStocksResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_stock_generator_service_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetStocksResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetStocksResponse) ProtoMessage() {}

func (x *GetStocksResponse) ProtoReflect() protoreflect.Message {
	mi := &file_stock_generator_service_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetStocksResponse.ProtoReflect.Descriptor instead.
func (*GetStocksResponse) Descriptor() ([]byte, []int) {
	return file_stock_generator_service_proto_rawDescGZIP(), []int{2}
}

func (x *GetStocksResponse) GetApi() string {
	if x != nil {
		return x.Api
	}
	return ""
}

func (x *GetStocksResponse) GetStocks() []*StockPrice {
	if x != nil {
		return x.Stocks
	}
	return nil
}

var File_stock_generator_service_proto protoreflect.FileDescriptor

var file_stock_generator_service_proto_rawDesc = []byte{
	0x0a, 0x1d, 0x73, 0x74, 0x6f, 0x63, 0x6b, 0x2d, 0x67, 0x65, 0x6e, 0x65, 0x72, 0x61, 0x74, 0x6f,
	0x72, 0x2d, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12,
	0x02, 0x76, 0x31, 0x1a, 0x1f, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x62, 0x75, 0x66, 0x2f, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x22, 0xa8, 0x01, 0x0a, 0x0a, 0x53, 0x74, 0x6f, 0x63, 0x6b, 0x50, 0x72,
	0x69, 0x63, 0x65, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52,
	0x02, 0x69, 0x64, 0x12, 0x16, 0x0a, 0x06, 0x74, 0x69, 0x63, 0x6b, 0x65, 0x72, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x06, 0x74, 0x69, 0x63, 0x6b, 0x65, 0x72, 0x12, 0x14, 0x0a, 0x05, 0x70,
	0x72, 0x69, 0x63, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x02, 0x52, 0x05, 0x70, 0x72, 0x69, 0x63,
	0x65, 0x12, 0x20, 0x0a, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e,
	0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74,
	0x69, 0x6f, 0x6e, 0x12, 0x3a, 0x0a, 0x0a, 0x6c, 0x61, 0x73, 0x74, 0x55, 0x70, 0x64, 0x61, 0x74,
	0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x74,
	0x61, 0x6d, 0x70, 0x52, 0x0a, 0x6c, 0x61, 0x73, 0x74, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x22,
	0x24, 0x0a, 0x10, 0x47, 0x65, 0x74, 0x53, 0x74, 0x6f, 0x63, 0x6b, 0x73, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x12, 0x10, 0x0a, 0x03, 0x61, 0x70, 0x69, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x03, 0x61, 0x70, 0x69, 0x22, 0x4d, 0x0a, 0x11, 0x47, 0x65, 0x74, 0x53, 0x74, 0x6f, 0x63,
	0x6b, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x10, 0x0a, 0x03, 0x61, 0x70,
	0x69, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x61, 0x70, 0x69, 0x12, 0x26, 0x0a, 0x06,
	0x73, 0x74, 0x6f, 0x63, 0x6b, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x0e, 0x2e, 0x76,
	0x31, 0x2e, 0x53, 0x74, 0x6f, 0x63, 0x6b, 0x50, 0x72, 0x69, 0x63, 0x65, 0x52, 0x06, 0x73, 0x74,
	0x6f, 0x63, 0x6b, 0x73, 0x32, 0x49, 0x0a, 0x0d, 0x53, 0x74, 0x6f, 0x63, 0x6b, 0x73, 0x53, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x38, 0x0a, 0x09, 0x47, 0x65, 0x74, 0x53, 0x74, 0x6f, 0x63,
	0x6b, 0x73, 0x12, 0x14, 0x2e, 0x76, 0x31, 0x2e, 0x47, 0x65, 0x74, 0x53, 0x74, 0x6f, 0x63, 0x6b,
	0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x15, 0x2e, 0x76, 0x31, 0x2e, 0x47, 0x65,
	0x74, 0x53, 0x74, 0x6f, 0x63, 0x6b, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x42,
	0x06, 0x5a, 0x04, 0x2e, 0x2f, 0x76, 0x31, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_stock_generator_service_proto_rawDescOnce sync.Once
	file_stock_generator_service_proto_rawDescData = file_stock_generator_service_proto_rawDesc
)

func file_stock_generator_service_proto_rawDescGZIP() []byte {
	file_stock_generator_service_proto_rawDescOnce.Do(func() {
		file_stock_generator_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_stock_generator_service_proto_rawDescData)
	})
	return file_stock_generator_service_proto_rawDescData
}

var file_stock_generator_service_proto_msgTypes = make([]protoimpl.MessageInfo, 3)
var file_stock_generator_service_proto_goTypes = []interface{}{
	(*StockPrice)(nil),            // 0: v1.StockPrice
	(*GetStocksRequest)(nil),      // 1: v1.GetStocksRequest
	(*GetStocksResponse)(nil),     // 2: v1.GetStocksResponse
	(*timestamppb.Timestamp)(nil), // 3: google.protobuf.Timestamp
}
var file_stock_generator_service_proto_depIdxs = []int32{
	3, // 0: v1.StockPrice.lastUpdate:type_name -> google.protobuf.Timestamp
	0, // 1: v1.GetStocksResponse.stocks:type_name -> v1.StockPrice
	1, // 2: v1.StocksService.GetStocks:input_type -> v1.GetStocksRequest
	2, // 3: v1.StocksService.GetStocks:output_type -> v1.GetStocksResponse
	3, // [3:4] is the sub-list for method output_type
	2, // [2:3] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_stock_generator_service_proto_init() }
func file_stock_generator_service_proto_init() {
	if File_stock_generator_service_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_stock_generator_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StockPrice); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_stock_generator_service_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetStocksRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_stock_generator_service_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetStocksResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_stock_generator_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   3,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_stock_generator_service_proto_goTypes,
		DependencyIndexes: file_stock_generator_service_proto_depIdxs,
		MessageInfos:      file_stock_generator_service_proto_msgTypes,
	}.Build()
	File_stock_generator_service_proto = out.File
	file_stock_generator_service_proto_rawDesc = nil
	file_stock_generator_service_proto_goTypes = nil
	file_stock_generator_service_proto_depIdxs = nil
}
