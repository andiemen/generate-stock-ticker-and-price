package v1

import (
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"

	"gitlab.com/andiemen/generate-stock-ticker-and-price/pkg/api/v1"
	"gitlab.com/andiemen/generate-stock-ticker-and-price/pkg/models"
)

const (
	// apiVersion is version of API is provided by server
	apiVersion = "v1"
)

// stocksServiceServer is implementation of v1.StocksServiceServer proto interface
type stocksServiceServer struct {
	db *gorm.DB
	v1.UnimplementedStocksServiceServer
}

// NewStocksServiceServer creates ToDo service
func NewStocksServiceServer(db *gorm.DB) v1.StocksServiceServer {
	return &stocksServiceServer{db: db}
}

// checkAPI checks if the API version requested by client is supported by server
func (s *stocksServiceServer) checkAPI(api string) error {
	// API version is "" means use current version of the service
	if len(api) > 0 {
		if apiVersion != api {
			return status.Errorf(codes.Unimplemented,
				"unsupported API version: service implements API version '%s', but asked for '%s'", apiVersion, api)
		}
	}
	return nil
}

// Read all stocks
func (s *stocksServiceServer) GetStocks(ctx context.Context, req *v1.GetStocksRequest) (*v1.GetStocksResponse, error) {
	// check if the API version requested by client is supported by server
	if err := s.checkAPI(req.Api); err != nil {
		return nil, err
	}
	
	var stockPrices []models.StockPrice
	priceSubQuery := s.db.Model(
		&models.Price{},
		).Select(
			"created_at, current",
		).Where(
			"stock_id = stocks.id",
		).Order(
			"created_at DESC",
		).Limit(1)
	result := s.db.Model(
		&models.Stock{},
		).Select(
			"stocks.id, stocks.ticker, COALESCE(prices.current, 0) as price, prices.created_at as last_update",
			).Joins(
				"LEFT JOIN LATERAL (?) prices on TRUE",
				priceSubQuery,
				).Scan(&stockPrices)

	list := make([]*v1.StockPrice, result.RowsAffected)

	for i, stockPrice := range stockPrices {
		sp := new(v1.StockPrice)
		sp.Id = int64(stockPrice.ID)
		sp.Ticker = stockPrice.Ticker
		sp.Description = stockPrice.Description
		sp.Price = stockPrice.Price
		sp.LastUpdate = timestamppb.New(stockPrice.LastUpdate)
		list[i] = sp
	}
	
	return &v1.GetStocksResponse{
		Api:   apiVersion,
		Stocks: list,
	}, nil
}
