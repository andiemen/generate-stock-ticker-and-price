package main

import (
	"gitlab.com/andiemen/generate-stock-ticker-and-price/initializers"
	"gitlab.com/andiemen/generate-stock-ticker-and-price/pkg/models"
)

func init() {
	initializers.LoadEnvVariables()
	initializers.ConnectToDB()
}

func main() {
	initializers.DB.AutoMigrate(&models.Stock{}, &models.Price{})
}