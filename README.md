# Introduction

This app generates and serves 100 random stock ticker names
and their latest prices

# CompileDaemon

CompileDaemon -command="./generate-stock-ticker-and-price"

## Set .env variables

Create .env file in base directory.

```
DATABASE_URL = postgresql://usr:pw@db:5432/postgres
POSTGRES_USER = 
POSTGRES_PASSWORD = 
POSTGRES_DB = stock_db
```

# Reference links

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [Go Micro Overview](https://micro.mu/docs/go-micro.html)
- [Go Micro Toolkit](https://micro.mu/docs/go-micro.html)

## Run Service

```shell
go run .\pkg\migrate\migrate.go
go build .\cmd\server\
.\server.exe -grpc-port=9090
```


## Query Service

```shell
go build .\cmd\client-grpc\
.\client-grpc.exe -server=localhost:9090
```
